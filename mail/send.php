<?php
//require_once __DIR__ . '/../extends/SendPulse/php/api/sendpulseInterface.php';
//require_once __DIR__ . '/../extends/SendPulse/php/api/sendpulse.php';

if($_POST['confirm'] != 'on')
{
  exit('Необходимо подтверждение согласия с условиями договора и политикой конфиденциальности.');
}

if(isset($_POST))
{
  $data = filter_var_array($_POST, [
                            'name'   => ['filter'  => FILTER_VALIDATE_REGEXP,
                            'options' => ['regexp' => "/^[a-za-яёЁ0-9\s]*/is"]
                           ],
                            'phone'  => ['filter'  => FILTER_VALIDATE_REGEXP,
                            'options' => ['regexp' => "/^[^a-zА-я]*/is"]
                           ],
                            'email'  => FILTER_SANITIZE_EMAIL,
                            'info'   => FILTER_SANITIZE_STRING
                          ]);

  $phone = (null != $data['phone']) ? '<p><b>Телефон</b>: ' . $data['phone'] . '</p>' : null;
  $email = (null != $data['email']) ? '<p><b>E-mail</b>: ' . $data['email'] . '</p>' : null;

  $to      = 'info@omnisoft.pro';
  $subject = 'Заявка на ' . $data['info'];
  $success_url = '/mail/thanks.htm';
  $boundary = md5(uniqid(time()));
  $headers = null;

  $headers .= 'Content-type: text/html; charset=UTF-8' . PHP_EOL;
  $headers .= "From: ". " omnisoft <" . $to . ">" . PHP_EOL;

  $message = '<html><head><title>Заявка с сайта</title></head><body>';

  $message .= '<p><b>Пользователь</b> <i>' . $data['name'] . '</i> оставил заявку на ' . $data['info'] . '.</p>';
  $message .= $phone.$email;

  $message .= '</body></html>';

  if(!mail($to, $subject, $message, $headers))
  {
  	exit('Ошибка отправки почты.');
  }

  // Добавляем мыло в адресную книгу SendPulse
  /*define( 'API_USER_ID', '4aa7036e1fddc87c8f91a23ec96197fe' );
  define( 'API_SECRET', '4de1a81645cef570972887d244c80276' );
  define( 'TOKEN_STORAGE', 'session' );

  $SPApiProxy = new SendpulseApi( API_USER_ID, API_SECRET, TOKEN_STORAGE );*/

  // Узнаём идентификатор адресной книги
  //var_dump($SPApiProxy->listAddressBooks());

  /*$email = [
     [
       'email' => $data['email'],
       'variables' => [
          'name'   => $data['name'],
          'phone' => $data['phone']
       ]
     ]
   ];

   $SPApiProxy->addEmails(1635669, $email);*/

   // Добавляем лид в Битрикс24
  /*if($data['email'] != null or $data['phone'] != null)
  {
    $TITLE              = 'Просто стрижка';
    $NAME               = $data['name'];
    $EMAIL              = $data['email'];
    $PHONE              = $data['phone'];
    $COMPANY_TITLE      = 'Просто стрижка';
    $SOURCE_DESCRIPTION = ucfirst($data['info']);
    $USER_ID            = 42;

    file_get_contents("https://feedback24.apps4b.ru/handler/leadadd/".
      "?client_key=s241izi1zi3xiz8zvwzc81ulw2tfyuyq".
      "&form_id=110".
      "&TITLE=".urlencode($TITLE).
      "&NAME=".urlencode($NAME).
      "&EMAIL=".urlencode($EMAIL).
      "&PHONE=".urlencode($PHONE).
      "&ASSIGNED_BY_ID=".$USER_ID.
      "&COMPANY_TITLE=".urlencode($COMPANY_TITLE).
      "&SOURCE_DESCRIPTION=".urlencode($SOURCE_DESCRIPTION)
    );
  }*/

  header('Location: '.$success_url);
  exit;

}
