jQuery(function() {
	$(".sidenav").sidenav();
	$('.scrollspy').scrollSpy({scrollOffset:10});
  $('.slider').slider();
  $('.parallax').parallax();
  $('.carousel').carousel();
  $('.modal').modal();

  $('.slick').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    dots: true,
    autoplaySpeed: 2000,
    responsive: [
    {
      breakpoint: 499,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 599,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 993,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4
      }
    }
    ]
  });

	//Behavior headers when scrolling
	$(window).on('scroll', (function() {

		if ($(this).scrollTop() >= 55){
			$('.navbar nav').removeClass('transparent').addClass('grey darken-4 shadow');
		}
		else {
			$('.navbar nav').removeClass('grey darken-4 shadow').addClass('transparent');
		}

	}));

	//Scroll top
	$('.brand-logo').on('click', function(){
		$('body, html').animate({
			scrollTop: 0
		}, 0);
	});

	var mixer = mixitup('#filter-content', {
    load: {
      sort: 'order:desc'
    },
    classNames: {
      block: 'filter',
      elementFilter: 'btn'
    },
    selectors: {
      target: '.item'
    }
  });

  var active = $('.filter .btn');

  $(active).each(function() {
  	if($(this).is('.filter-control-active')) {
  		$(this).removeClass('btn-flat');
  	}

    $(this).click(function() {
    	$(this).parent().find('.btn').addClass('btn-flat');
    	$(this).removeClass('btn-flat');
    });
  });

  //##########################################
  // Яндекс.карты
  //##########################################
  function yaMap(){
    ymaps.ready(init);

    function init(){
      var target = $('#map');
      // Строка с адресом, который нужно геокодировать
      var address = ($(target).data('address')) ? $(target).data('address') : "Екатеринбург";
      // Ищем координаты указанного адреса
      var geocoder = ymaps.geocode(address);

      
      // После того, как поиск вернул результат, вызывается callback-функция
      geocoder.then(
        function (res) {
          // координаты объекта
          var coordinates = res.geoObjects.get(0).geometry.getCoordinates();
          // Создаем карту
          var map = new ymaps.Map("map", {
              center: coordinates, // центр карты - координаты найденного объекта
              zoom: 17, // коэффициент масштабирования
      
              // элементы управления картой
              // список элементов можно посмотреть на этой странице
              // https://tech.yandex.ru/maps/doc/jsapi/2.1/dg/concepts/controls-docpage/
              controls: [
                  'typeSelector', // переключатель отображаемого типа карты
                  'zoomControl' // ползунок масштаба
              ]
          });

          map.behaviors.disable('scrollZoom');

          if($(this).parents('html').hasClass('mobile'))
          {
            map.behaviors.disable('drag');
          }

          var html  = '<div class="popup">';
              html +=   '<div class="popup-text">';
              html +=     '<h5 style="padding-top: 0.2em; padding-bottom: 0.1em">Omnisoft</h5>';
              html +=     '<div style="padding-bottom: 0.2em"><b>Адрес</b>: ' + address + '</div>';
              html +=     '<div style="padding-bottom: 0.2em"><b>Режим работы</b>: Пн-Пт с 10:00 до 19:00</div>';
              html +=     '<div style="padding-bottom: 0.2em"><b>Телефон</b>: +7 (343) 328-06-20</div>';
              html +=     '<div style="padding-bottom: 0.6em"><b>Email</b>: <a href="mailto:info@omnisoft.pro">info@omnisoft.pro</a></div>';
              html +=   '</div>';
              html += '</div>';

          // Добавление метки (Placemark) на карту
          // Подробнее про метки здесь https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/Placemark-docpage/
          
          var placemark = new ymaps.Placemark(
              coordinates, // коодирнаты метки
      
              // объект с данными метки
              // подробнее https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/Placemark-docpage/#param-properties
              {
                  'hintContent': address, // всплывающая подсказка (выводим адрес объекта)
                  'balloonContent': html, // содержимое балуна (выводим время работы)
              },
              // подробнее https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/Placemark-docpage/#param-options
              {
                  // варианты стилей https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/option.presetStorage-docpage/
                  'preset': 'islands#bluePocketIcon'
              }
          );

          map.geoObjects.add(placemark);

          placemark.balloon.open();
        });
    }
  }

  yaMap();
      

});
